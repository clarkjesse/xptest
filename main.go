// BUILD:  go build -buildmode=c-shared -o win.xpl
package main

/*
#cgo CFLAGS: -DIBM -DSIMDATA_EXPORTS -DXPLM200=1 -DXPLM210=1 -DXPLM300=1 -DXPLM301=1
#cgo CFLAGS: -I${SRCDIR}/SDK/CHeaders/XPLM/
#cgo CFLAGS: -I${SRCDIR}/SDK/CHeaders/Wrappers/
#cgo CFLAGS: -I${SRCDIR}/SDK/CHeaders/Widgets/
#cgo LDFLAGS: -L${SRCDIR}/SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64
#include "XPLMUtilities.h"
 */
import "C"
import "unsafe"

//export go_callback_int
func go_callback_int()  {

	Log("ZBS!!!!!!!!!!!!!!!")
	//m := new(MenuAPI)
	//m.CreateTESTMenu()
	menuContainerId := AppendMenuItem(FindPluginsMenu(), "Resentium Menu", nil, true)
	menuId := CreateMenu("Resentium Menu", FindPluginsMenu(), menuContainerId, func(menuRef, itemRef interface{}) {

	}, nil)

	AppendMenuItem(menuId, "Toggle Settings",  "Menu Item 1", true)
	AppendMenuSeparator(menuId)
	AppendMenuItem(menuId, "Toggle Shortcuts",  "Menu Item 2", true)
	Log("OK")
}

func main() {

}

func Log(msg string)  {
	cStr := C.CString(msg)
	defer C.free(unsafe.Pointer(cStr))
	C.XPLMDebugString(cStr)
}

