package main

/*
#cgo CFLAGS: -DIBM=1
#cgo CFLAGS: -I${SRCDIR}/SDK/CHeaders/XPLM/
#cgo CFLAGS: -I${SRCDIR}/SDK/CHeaders/Wrappers/
#cgo CFLAGS: -I${SRCDIR}/SDK/CHeaders/Widgets/
#cgo LDFLAGS: -L${SRCDIR}/SDK/Libraries/Win -lXPLM_64 -lXPWidgets_64

#include "D:/DEV/_ENVIRONMENT_/GoWorkspace/src/xptest/SDK/CHeaders/XPLM/XPLMMenus.h"
#include <string.h>
#include <windows.h>

int g_menu_container_idx; // The index of our menu item in the Plugins menu
XPLMMenuID g_menu_id; // The menu container we'll append all our menu items to
void menu_handler(void *, void *);

PLUGIN_API int XPluginStart(
						char *		outName,
						char *		outSig,
						char *		outDesc)
{
	strcpy(outName, "SkyWarden");
	strcpy(outSig, "com.skyready.skywarden");
	strcpy(outDesc, "A sample plug-in that demonstrates and exercises the X-Plane menu API.");

	go_callback_int();

	return 1;
}

PLUGIN_API void	XPluginStop(void)
{
	// Since we created this menu, we'll be good citizens and clean it up as well
	XPLMDestroyMenu(g_menu_id);
	// If we were able to add a command to the aircraft menu, it will be automatically removed for us when we're unloaded
}

PLUGIN_API void XPluginDisable(void) { }
PLUGIN_API int XPluginEnable(void) { return 1; }
PLUGIN_API void XPluginReceiveMessage(XPLMPluginID inFrom, int inMsg, void * inParam) { }

void menu_handler(void * in_menu_ref, void * in_item_ref)
{
	if(!strcmp((const char *)in_item_ref, "Menu Item 1"))
	{
		XPLMCommandOnce(XPLMFindCommand("sim/operation/toggle_settings_window"));
	}
	else if(!strcmp((const char *)in_item_ref, "Menu Item 2"))
	{
		XPLMCommandOnce(XPLMFindCommand("sim/operation/toggle_key_shortcuts_window"));
	}
}
 */
import "C"